#!/usr/bin/env ruby

puts "loading libraries..."
require 'net/http'
require 'json'
require 'securerandom'
require 'cgi'

def buildRequest(accessToken, uri)
  req = Net::HTTP::Get.new(URI(uri))
  req['Cookie'] = ('oauthpennidinh=' + accessToken)
 
  return req
end

def downloadFile(originalFile, localFilePath, accessToken)
  if originalFile.include? "/home-security/home-security"
      puts "Temporary hack to remove extra file prefix" 
      originalFile = originalFile.sub '/home-security', ''
  end
  req = buildRequest accessToken, ('https://pennidinh.com/secure/' + originalFile)
  Net::HTTP.start('pennidinh.com', 443, {:use_ssl => true}) do |http|
    puts "Starting download..."
    $stdout.flush
    http.request req do |response|
      f = File.new(localFilePath, 'w')
      response.read_body do |chunk|
          puts "Chunk recieved"
          f.write(chunk)
      end
      f.close
    end
  end
  puts "Download complete"
  $stdout.flush
end

def deleteOriginalFile(originalFile, accessToken)
  req = buildRequest accessToken, ('https://pennidinh.com/secure-node-js/deleteFile.js?file=' + CGI::escape(originalFile))
  response = Net::HTTP.start('pennidinh.com', 443, {:use_ssl => true}) do |http|
    puts "Requesting delte of corrupt file: #{originalFile}"
    $stdout.flush
    http.request req 
  end
  if response.code == '200'
    puts "Delete successful"
  else
    puts "Delete failed"
  end
end

def uploadThumbnail(originalFile, localFile, thumbnailSuffix, accessToken)
  puts "Uploading thumbnail for #{originalFile} from #{localFile} with thumbnail suffix #{thumbnailSuffix}"

  response = Net::HTTP.start('pennidinh.com', 443, {:use_ssl => true}) do |http|
    req = Net::HTTP::Post.new(URI("https://pennidinh.com/secure-node-js/uploadThumbnail.js?originalFile=#{CGI::escape originalFile}&thumbnailSuffix=#{CGI::escape thumbnailSuffix}"))
    req['Cookie'] = ('oauthpennidinh=' + accessToken)
    req.body = File.read(localFile)

    http.request(req)
  end

  if response.code != '200'
    raise 'Non 200 status code returned from thumbnail upload: ' + response.body
  end
end

puts "daemon starting up..."
while true do
  puts Time.new.inspect
  $stdout.flush
  begin
    accessTokenStringObject = Net::HTTP.get(URI('https://pennidinh.com/secure-node-js/getAccessToken.js?appSecretKey=fef876a6-bf32-4bd5-8dc8-9728836a1aef&clientId=PenniDinh'))
    accessTokenObject = JSON.parse(accessTokenStringObject)
    accessToken = accessTokenObject['accessToken']
    req = buildRequest accessToken, 'https://pennidinh.com/secure-node-js/popVideoThumbnail.js'
    response = Net::HTTP.start('pennidinh.com', 443, {:use_ssl => true}) do |http|
      http.read_timeout = 130
      puts "Starting blocking request to get next thumbnail..."
      $stdout.flush
      http.request req 
    end
    if response.code == '200'
      originalFile = CGI::unescape(response.body)
      
      if originalFile.include? 'thumbnail'
	 next
      end

      puts "Thumbnail available: #{originalFile}"
      tempFileName = '/tmp/thumbnail-download-' + SecureRandom.random_number(100000000).to_s

      downloadFile originalFile, tempFileName, accessToken
      
      nuonce = SecureRandom.random_number(100000000).to_s
      if originalFile.include? "mp4"
	puts "Generating video thumbnails..."
        output = `ffmpeg -i #{tempFileName} -t 15 -r .2 -filter:v scale="260:-1" /tmp/thumbnail-#{nuonce}-%05d.png`
	puts output
	if output.include?('Invalid data found when processing input')
	  puts "Corrupt original file"
	  deleteOriginalFile originalFile, accessToken
	  next
        end
	puts "Thumbnail generation complete"

	localThumbnailFileName1 = "/tmp/thumbnail-#{nuonce}-00001.png"
	localThumbnailFileName2 = "/tmp/thumbnail-#{nuonce}-00002.png"
	localThumbnailFileName3 = "/tmp/thumbnail-#{nuonce}-00003.png"
	uploadThumbnail originalFile, localThumbnailFileName1, '-thumbnail-00001.png', accessToken
	uploadThumbnail originalFile, localThumbnailFileName2, '-thumbnail-00002.png', accessToken
	uploadThumbnail originalFile, localThumbnailFileName3, '-thumbnail-00003.png', accessToken

        File.delete(localThumbnailFileName1)
        File.delete(localThumbnailFileName2)
        File.delete(localThumbnailFileName3)
        puts "Local thumbnail files deleted"
      elsif originalFile.include? "jpg"
        puts "Generating image thumbnails"
	localThumbnailFileName = "/tmp/thumbnail-#{nuonce}.png"
        output = `convert -resize 260x144 jpg:#{tempFileName} png:#{localThumbnailFileName}`
	puts output
        if output.include?('insufficient image data in file')
	  puts "Corrupt original image file"
	  deleteOriginalFile originalFile, accessToken
	  next
	end
	puts "Thumbnail generation complete"

	uploadThumbnail originalFile, localThumbnailFileName, '-thumbnail.png', accessToken

        File.delete(localThumbnailFileName)
      else
        raise "Unexpected filetype (not mp4 for jpg) for file: #{originalFile}"
      end

      File.delete(tempFileName)
      puts "Local original file copy deleted"
    else
      puts 'No thumbnail available'
      puts 'Sleeping 1 second...'
      $stdout.flush
      sleep 1 
    end
  rescue Exception => e 
    puts "Error while executing: " + e.to_s + e.backtrace.to_s
    puts 'Sleeping 1 second...'
    $stdout.flush
    sleep 1 
  end
end

#!/usr/bin/ruby

require "net/http"
require "uri"
require 'json'
require 'curb'
require 'cgi'

DEVICE_ID = File.read(File.dirname(__FILE__) + '/deviceId.conf').chomp

puts "Current time: " + Time.new.to_s

secretKey = '55b58dd2-bac4-4fc5-b302-bd78172c9954'
accessToken = JSON.parse(Curl.get('https://pennidinh.com/secure-node-js/getAccessToken.js?clientId=PenniDinh&appSecretKey=' + secretKey).body_str)['accessToken'].chomp
accessTokenCookie = "oauthpennidinh=#{CGI.escape accessToken}"

http = Curl.get("https://pennidinh.com/secure-node-js/getHomeHeaters.json") do |http|
    http.headers['Cookie'] = accessTokenCookie
end
response = http.body_str
puts 'getPlugDevices.json response from hub: ' + response
responseAsJson = JSON.parse(response)

if (!responseAsJson.kind_of?(Array))
  raise "Expected response to be of type Array!"
end

thisDevice = responseAsJson.select {|item| DEVICE_ID == item['thermosatDeviceID']}
if (thisDevice.size != 1)
  raise "Expected thisDevice.size to be 1 but was + #{thisDevice.size}" 
end

puts thisDevice
heaterTurnedOff = !thisDevice[0]['power']
if heaterTurnedOff
  puts "The heater is set to off"
end

currentRangeHighFer = thisDevice[0]['currentHighRangeTempFer'].to_f
puts "currentRangeHighFer: #{currentRangeHighFer}"
currentRangeLowFer = thisDevice[0]['currentLowRangeTempFer'].to_f
puts "currentRangeLowFer: #{currentRangeLowFer}"
powerSwitchIPAddress = thisDevice[0]['powerSwitchIPAddress']
puts "powerSwitchIPAddress: #{powerSwitchIPAddress}"
powerSwitchIPPort = thisDevice[0]['powerSwitchIPPort']
puts "powerSwitchIPPort: #{powerSwitchIPPort}"

a = `cat /sys/bus/w1/devices/28-*/w1_slave`
puts "\"cat /sys/bus/w1/devices/28-*/w1_slave\" output: " + a

tempCelsRaw = a.split('t=')[1]

puts "Raw Temp Value : " + tempCelsRaw

tempCels = tempCelsRaw.to_i / 1000.0

puts "Temp (C) : " + tempCels.to_s

tempFer = tempCels * 9 / 5 + 32

puts "Temp (F) : " + tempFer.to_s

puts "Temp range #{currentRangeLowFer} to #{currentRangeHighFer}"

tooWarm = tempFer > currentRangeLowFer
if tooWarm
  puts "The temp is too high"
end
if heaterTurnedOff || tooWarm
  puts 'Turning the outlet off...'
  http = Curl.get("https://pennidinh.com/secure-node-js/setPlugDevice.json?hostIP=#{thisDevice[0]['powerSwitchIPAddress']}&hostPort=#{thisDevice[0]['powerSwitchIPPort']}&value=false") do |http|
      http.headers['Cookie'] = accessTokenCookie
  end
  puts http.body_str
elsif tempFer < currentRangeLowFer
  puts 'Temp too low. Turning the outlet on...'
  http = Curl.get("https://pennidinh.com/secure-node-js/setPlugDevice.json?hostIP=#{thisDevice[0]['powerSwitchIPAddress']}&hostPort=#{thisDevice[0]['powerSwitchIPPort']}&value=true") do |http|
      http.headers['Cookie'] = accessTokenCookie
  end
  puts http.body_str
end

puts "Updating hub with current temp fer:: #{tempFer}"
puts "Updating hub with current temp cels: #{tempCels}"

http = Curl.get("https://pennidinh.com/secure-node-js/setHomeHeater.json?powerSwitchIPAddress=#{powerSwitchIPAddress}&powerSwitchIPPort=#{powerSwitchIPPort}&thermosatDeviceID=#{DEVICE_ID}&newCurrentTempFer=#{tempFer}&newCurrentTempCels=#{tempCels}") do |http|
    http.headers['Cookie'] = accessTokenCookie
end
puts http.body_str
